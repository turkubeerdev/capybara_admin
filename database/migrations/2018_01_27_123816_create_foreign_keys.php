<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

    public function up()
    {
        Schema::table('pylons', function(Blueprint $table) {
            $table->foreign('hub_id')->references('id')->on('hubs')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
        Schema::table('sensors', function(Blueprint $table) {
            $table->foreign('sensor_type_id')->references('id')->on('sensor_type')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('sensors', function(Blueprint $table) {
            $table->foreign('pylon_id')->references('id')->on('pylons')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    public function down()
    {
        Schema::table('pylons', function(Blueprint $table) {
            $table->dropForeign('pylons_hub_id_foreign');
        });
        Schema::table('sensors', function(Blueprint $table) {
            $table->dropForeign('sensors_sensor_type_id_foreign');
        });
        Schema::table('sensors', function(Blueprint $table) {
            $table->dropForeign('sensors_pylon_id_foreign');
        });
    }
}