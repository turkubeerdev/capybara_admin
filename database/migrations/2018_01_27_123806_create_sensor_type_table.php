<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSensorTypeTable extends Migration {

	public function up()
	{
		Schema::create('sensor_type', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->text('description')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('sensor_type');
	}
}