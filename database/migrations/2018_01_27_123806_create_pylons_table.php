<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePylonsTable extends Migration {

	public function up()
	{
		Schema::create('pylons', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->text('descrption')->nullable();
			$table->smallInteger('depth')->nullable();
			$table->string('location_readable')->nullable();
			$table->string('lat')->nullable();
			$table->string('long')->nullable();
			$table->integer('hub_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('pylons');
	}
}