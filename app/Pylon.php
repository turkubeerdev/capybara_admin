<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Pylon
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $name
 * @property string|null $descrption
 * @property int|null $depth
 * @property string|null $location_readable
 * @property string|null $lat
 * @property string|null $long
 * @property int $hub_id
 * @property \Illuminate\Database\Eloquent\Collection|\App\Sensor[] $sensors
 * @property-read \App\Hub $hub
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereDescrption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereLocationReadable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereSensors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pylon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $sensor
 */
class Pylon extends Model 
{
    use CrudTrait;
    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'pylons';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'depth', 'location_readable', 'lat', 'long', 'sensor');
    protected $hidden = ['id','created_at', 'updated_at'];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /**
     * @return string
     */
    public function manage()
    {
        $url =  backpack_url('hub') . '/' . $this->hub_id . '/pylon/' . $this->id . '/manage';
        $format = '<a class="btn btn-xs btn-default" href="%s"  data-toggle="tooltip" title="See detailed view of pylon info and manage sensors."><i class="fa fa-search"></i>Manage</a>';

        return sprintf($format, $url);
    }




    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sensors()
    {
        return $this->hasMany(Sensor::class);
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getSensorAttribute()
    {
        return $this->sensors()->count();
    }

}