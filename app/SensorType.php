<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\SensorType
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $name
 * @property string|null $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Sensor[] $sensors
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SensorType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SensorType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SensorType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SensorType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SensorType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SensorType extends Model
{

    use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'sensor_type';
    public $timestamps = true;
    protected $fillable = ['name', 'description'];


    /*
     |--------------------------------------------------------------------------
     | FUNCTIONS
     |--------------------------------------------------------------------------
     */


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function sensors()
    {
        return $this->hasMany(Sensor::class);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}