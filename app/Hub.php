<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;


/**
 * App\Hub
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $name
 * @property string|null $location_readable
 * @property string|null $lat
 * @property string|null $long
 * @property string|null $descrption
 * @property int $setup_complete
 * @property int $device_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pylon[] $pylons
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Sensor[] $sensors
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereDescrption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereLocationReadable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereSetupComplete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hub whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Device $device
 */
class Hub extends Model 
{

    use CrudTrait;


    protected $table = 'hubs';
    public $timestamps = true;
    protected $fillable = array('name', 'location_readable', 'lat', 'long', 'descrption', 'setup_complete', 'device_id');

    public function pylons()
    {
        return $this->hasMany(Pylon::class);
    }

    public function sensors()
    {
        return $this->hasMany(Sensor::class);
    }


    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return string
     */
    public function manage()
    {
        $url =  backpack_url('hub') . '/' . $this->id . '/manage';
        $format = '<a class="btn btn-xs btn-default" href="%s"  data-toggle="tooltip" title="See detailed view of hubs info and manage pylons."><i class="fa fa-search"></i>Manage</a>';

        return sprintf($format, $url);
    }

    public function getPylonTemperature($id)
    {
        $device = Device::find($this->device_id);

        $readings = $device->readings()->orderBy('created_at', 'desc')->first();

        if (!$readings) {
            return 0;
        }
        $combinedReadings = 0;


        foreach ($readings->sensors as $sensor) {
            $combinedReadings += $sensor['value'];
        }

        return $combinedReadings/count($readings->sensors);


    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function device()
    {
        return $this->belongsTo(Device::class);
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


}