<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Readings extends Eloquent
{
    protected $connection = 'mongodb';

    protected $guarded = ['_id'] ;

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
    
}
