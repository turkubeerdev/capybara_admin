<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\SensorRequest as StoreRequest;
use App\Http\Requests\SensorRequest as UpdateRequest;

class SensorController extends CrudController
{

    public function setup()
    {
        $this->crud->setModel('App\Sensor');
        $this->crud->setRoute(backpack_url('sensor'));
        $this->crud->setEntityNameStrings('sensor', 'sensors');
        $this->crud->with('type');

        $this->crud->addField([
            'name' => 'uuid',
            'label' => 'UUID',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 '
            ]

        ]);
        $this->crud->addColumn([
            'name' => 'uuid',
            'label' => 'UUID'
        ]);

        $this->crud->addField([
            'name' => 'position',
            'label' => 'Position',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-offset-2 col-md-4 '
            ]

        ]);
        $this->crud->addColumn([
            'name' => 'position',
            'label' => 'Position'
        ]);

        $this->crud->addField([
            'label'     => 'Sensor Type',
            'type'      => 'select2',
            'name'      => 'sensor_type_id', // the db column for the foreign key
            'entity'    => 'type', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ], // extra HTML attributes for the field wrapper - mostly for resizing fields
        ]);
        $this->crud->addColumn([
            'label' => 'Sensor Type',
            'type' => 'select',
            'name' => 'sensor_type_id',
            'entity' => 'type',
            'attribute' => 'name',

        ]);



    }




}

?>