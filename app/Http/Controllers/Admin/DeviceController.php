<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    public function index()
    {
        $devices = Device::unassociated();

        return  view('admin.device.index')->with('devices',$devices);
    }

    public function show(Device $device)
    {
        return $device->name;
    }
}
