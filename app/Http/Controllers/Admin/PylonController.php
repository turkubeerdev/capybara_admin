<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\HubController;
use App\Hub;
use App\Pylon;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class PylonController extends CrudController
{

    public function setup()
    {
        $this->crud->setModel('App\Pylon');
        $this->crud->setRoute(backpack_url('pylon'));
        $this->crud->setFromDb();
        $this->crud->setEntityNameStrings('pylon', 'pylons');
        $this->crud->allowAccess('list');
        $this->crud->with('sensors');

    }
    public function manageHubView(Hub $hub, Pylon $pylon)
    {

        // cycle through columns
        foreach ($this->crud->columns as $key => $column) {
            // remove any autoset relationship columns
            if (array_key_exists('model', $column) && array_key_exists('autoset', $column) && $column['autoset']) {
                $this->crud->removeColumn($column['name']);
            }
        }

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($hub->id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = trans('backpack::crud.preview') . ' ' . $this->crud->entity_name;

        // remove preview button from stack:line
        $this->crud->removeButton('preview');
        $this->crud->removeButton('delete');

        //Create Pylon Crud
        $pylon_crud = app(PylonSensorController::class)->getPylonCrud();

        //dd($this->crud, $pylon_crud->route);

        return view('admin.hub.manage', ['hub' => $hub, 'crud' => $this->data['crud'], 'entry' => $this->data['entry'], 'pylon_crud' => $pylon_crud]);
    }



}

?>