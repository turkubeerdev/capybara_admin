<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class SensorTypeController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\SensorType');
        $this->crud->setRoute(backpack_url('sensor_type'));
        $this->crud->setEntityNameStrings('sensor type', 'sensor typess');
        $this->crud->setFromDb();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        return parent::storeCrud();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param int $id
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}
