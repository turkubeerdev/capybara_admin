<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\Hub;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function show($hub)
    {
        $hub = Hub::find($hub);
        $readings = Device::find($hub->device_id)->readings()->get();

        $formatted_readings = [];
        foreach ($readings->first()->sensors as $uuid => $sensor ) {
            $formatted_readings += [$sensor['uuid'] => []];
        }
        foreach ($readings->pluck('sensors') as $reading) {
            foreach ($reading as $sensor) {
                array_push($formatted_readings[$sensor['uuid']], $sensor['value']);
            }
        }

        $timestamp = $readings->pluck('created_at');
        $timestamp->transform(function($item, $key){
            return $item->toTimeString();
        });
        //dd($formatted_readings);
        $chart = \Charts::multi('spline', 'highcharts')
            ->title('Temperature Time Series')
            ->template('material')
            // You could always set them manually
            //->colors(['rgba(68, 170, 213, 0.1)', 'rgba(68, 170, 213, 0.1)', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->datasets($formatted_readings)
            // Setup what the values mean
            ->labels($timestamp);
        return view('admin.dashboard.show', ['chart' => $chart]);
    }
}
