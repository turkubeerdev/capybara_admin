<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\PylonController;
use App\Hub;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class HubPylonController extends PylonController
{

    protected $hub_id;

    public function setup()
    {

        parent::setup();
        $this->crud->setFromDb();
        // get the hub_id parameter
        if (is_string(\Route::current()->parameter('hub'))) {
            $hub_id = \Route::current()->parameter('hub');
        } else {
            $hub_id = \Route::current()->parameter('hub')->id;
        }
        $this->crud->allowAccess(['list', 'create', 'delete']);

        // set a different route for the admin panel buttons
        $this->crud->setRoute("admin/hub/" . $hub_id . "/pylon");

        // show only that user's posts
        $this->crud->addClause('where', 'hub_id', $hub_id, '==');

        $this->crud->addButtonFromModelFunction('line', 'Pylon Details', 'manage', 'beginning');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param int $id
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Hub $hub, Request $request)
    {
        $hub->pylons()->create($request->all());

        return redirect()->back();
    }


    public function getPylonCrud()
    {
        $this->setup();

        return $this->crud;
    }


}

?>