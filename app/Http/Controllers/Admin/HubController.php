<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\Hub;
use App\Pylon;
use Backpack\CRUD\app\Http\Controllers\CrudController;


use Illuminate\Http\Request;

class HubController extends CrudController
{

    public function setup()
    {
        $this->crud->setModel('App\Hub');
        $this->crud->setRoute(backpack_url('hub'));
        $this->crud->setEntityNameStrings('hub', 'hubs');
        $this->crud->allowAccess('list');
        $this->crud->with('pylons');
        $this->crud->allowAccess('details_row');
        $this->crud->enableDetailsRow();
        $this->crud->setDetailsRowView('admin.hub.detail');
        //$this->crud->allowAccess('show'); //uncomment if you want to have a show view of the hub.

        $this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'device_id',
            'label' => 'Device Id',
            'type' => 'number',
        ]);

        $this->crud->addButtonFromModelFunction('line', 'Hub Details', 'manage', 'beginning');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        return parent::storeCrud();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param int $id
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }

    /**
     * @param Hub $hub
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageHubView(Hub $hub)
    {

        // cycle through columns
        foreach ($this->crud->columns as $key => $column) {
            // remove any autoset relationship columns
            if (array_key_exists('model', $column) && array_key_exists('autoset', $column) && $column['autoset']) {
                $this->crud->removeColumn($column['name']);
            }
        }

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($hub->id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = trans('backpack::crud.preview') . ' ' . $this->crud->entity_name;

        // remove preview button from stack:line
        $this->crud->removeButton('preview');
        $this->crud->removeButton('delete');

        //Create Pylon Crud
        $pylon_crud = app(HubPylonController::class)->getPylonCrud();

        //dd($pylon_crud);

        return view('admin.hub.manage', ['hub' => $hub, 'crud' => $this->data['crud'], 'entry' => $this->data['entry'], 'pylon_crud' => $pylon_crud]);
    }

    public function showDetailsRow($id)
    {
        $this->crud->hasAccessOrFail('details_row');

        $this->data['entry'] = $this->crud->getEntry($id)->pylons();
        $this->data['crud'] = $this->crud;
        $this->data['columns'] = Pylon::getModel()->getFillable();

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('admin.hub.detail', $this->data);

    }




}

?>