<?php

namespace App\Http\Controllers\Admin;

use App\Hub;
use App\Pylon;
use Illuminate\Http\Request;

class PylonSensorController extends SensorController
{

    protected $hub_id;

    public function setup()
    {

        parent::setup();
        $this->crud->setFromDb();
        //dd(\Route::current());
        // get the hub_id parameter
        if (is_string(\Route::current()->parameter('hub'))) {
            $hub_id = \Route::current()->parameter('hub');
        } else {
            $hub_id = \Route::current()->parameter('hub')->id;
        }
        // get the hub_id parameter
        if (is_string(\Route::current()->parameter('pylon'))) {
            $pylon_id = \Route::current()->parameter('pylon');
        } else {
            $pylon_id = \Route::current()->parameter('pylon')->id;
        }
        $this->crud->allowAccess(['list', 'create', 'delete']);

        // set a different route for the admin panel buttons
        $this->crud->setRoute("admin/hub/" . $hub_id . "/pylon/" . $pylon_id . "/sensor");

        // show only that user's posts
        $this->crud->addClause('where', 'pylon_id', $pylon_id, '==');

        $this->crud->addColumn([
            'label' => 'Sensor Type',
            'type' => 'select',
            'name' => 'sensor_type_id',
            'entity' => 'type',
            'attribute' => 'name',

        ]);

    }




    public function getPylonCrud()
    {
        $this->setup();

        return $this->crud;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @internal param int $id
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Hub $hub, Pylon $pylon, Request $request)
    {
        //dd($request->all());
        $sensor = $pylon->sensors()->make($request->all());
        $sensor->hub_id = $hub->id;
        $sensor->save();


        return redirect('admin/hub/' . $hub->id . '/pylon/' . $pylon->id . '/manage');
    }

    public function search()
    {
        $this->crud->hasAccessOrFail('list');

        $totalRows = $filteredRows = $this->crud->count();

        // if a search term was present
        if ($this->request->input('search') && $this->request->input('search')['value']) {
            // filter the results accordingly
            $this->crud->applySearchTerm($this->request->input('search')['value']);
            // recalculate the number of filtered rows
            $filteredRows = $this->crud->count();
        }

        // start the results according to the datatables pagination
        if ($this->request->input('start')) {
            $this->crud->skip($this->request->input('start'));
        }

        // limit the number of results according to the datatables pagination
        if ($this->request->input('length')) {
            $this->crud->take($this->request->input('length'));
        }

        // overwrite any order set in the setup() method with the datatables order
        if ($this->request->input('order')) {
            $column_number = $this->request->input('order')[0]['column'];
            if ($this->crud->details_row) {
                $column_number = $column_number - 1;
            }
            $column_direction = $this->request->input('order')[0]['dir'];
            $column = $this->crud->findColumnById($column_number);

            if ($column['tableColumn']) {
                $this->crud->orderBy($column['name'], $column_direction);
            }
        }
        $entries = $this->crud->getEntries();


        return $this->crud->getEntriesAsJsonForDatatables($entries, $totalRows, $filteredRows);
    }

}

?>