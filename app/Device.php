<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Device
 *
 * @property int $id
 * @property string $name
 * @property string $password
 * @property int|null $client_id
 * @property string|null $location
 * @property string|null $ip_address
 * @property string|null $system_info
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Readings[] $readings
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereSystemInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Device whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Device extends Model
{
    protected $connection = 'device_sqlite';

    use HybridRelations;

    public function readings()
    {
        return $this->hasMany(Readings::class);
    }

    public function hubQuery()
    {
        return Hub::where('device_id', '=', $this->id)->newQuery();
    }

    public function hub()
    {
        return Hub::where('device_id', '=', $this->id)->get();
    }

    public static function unassociated()
    {
        $devices = Device::all();
        $hubs_id = Hub::all()->pluck('device_id');

        return $devices->whereNotIn('id', $hubs_id);
    }


}
