<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Sensor
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $uuid
 * @property int|null $position
 * @property int $hub_id
 * @property int $pylon_id
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property-read \App\Hub $hub
 * @property-read \App\Pylon $pylon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor wherePylonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereUuid($value)
 * @mixin \Eloquent
 * @property int $sensor_type_id
 * @property-read \App\SensorType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sensor whereSensorTypeId($value)
 */
class Sensor extends Model 
{
    use CrudTrait;
    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'sensors';
    public $timestamps = true;
    protected $fillable = array('uuid', 'position', 'sensor_type_id');

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function pylon()
    {
        return $this->belongsTo(Pylon::class);
    }

    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }

    public function type()
    {
        return $this->belongsTo(SensorType::class, 'sensor_type_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


}