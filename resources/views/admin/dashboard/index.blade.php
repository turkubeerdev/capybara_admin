@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::base.dashboard') }}<small>{{ trans('backpack::base.first_page_you_see') }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">{{ trans('backpack::base.login_status') }}</div>
                </div>

                <div class="box-body">
                    @foreach(\App\Hub::where('setup_complete', '=', true)->get() as $device)
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{ $device->name }}</h3>
                                    @foreach($device->pylons()->get() as $pylon)
                                        <p>{{$pylon->name}}: {{round( $device->getPylonTemperature($pylon->id) , 2)}}  &#8451;</p>

                                    @endforeach
                                </div>
                                <div class="icon">
                                    <i class="fa fa-asterisk"></i>
                                </div>
                                <a href="{{ url('admin/dashboard/' . $device->id ) }}" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
