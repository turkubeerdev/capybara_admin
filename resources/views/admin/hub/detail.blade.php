<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
    <div class="row">
        <div class="col-md-12">
            <table id="detailTable" class="detailsTable">
                @if($entry->get()->count())
                    <thead>
                    <tr>
                        {{-- Table columns --}}
                        @foreach ($columns as $column )
                            <th data-orderable="true">
                                {{ $column }}
                            </th>
                        @endforeach
                    </tr>
                    </thead>

                    <tbody>



                    @foreach($entry->get() as $pylon)
                        <tr>
                            @foreach($pylon->toArray() as $pylonData)
                                <td>{{$pylonData}}</td>
                            @endforeach
                        </tr>
                    @endforeach


                    </tbody>
                @else
                    No pylons
                @endif
            </table>
        </div>
    </div>
</div>
<div class="clearfix"></div>



<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("#detailTable")
            .dataTable( {
                "paging":   false,
                "ordering": false,
                "info":     false,
                searching: false,
                /*TODO make the columns field dynamically set from db*/
                "columns": [
                    null,
                    {"width": "30%"},
                    null,
                    null,
                    null,
                    null,
                    null
                ]
            } );
    });

</script>

