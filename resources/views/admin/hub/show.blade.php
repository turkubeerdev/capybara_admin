@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::base.dashboard') }}
            <small>{{ trans('backpack::base.first_page_you_see') }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Info: {{ $hub->name }}</div>
                </div>

                <div class="box-body">
                    <table class="table table-striped table-bordered">
                        <tbody>
                        @foreach($hub->toArray() as $hubKey => $hubValue)
                            <tr>
                                <td>
                                    <strong>{{ $hubKey }}</strong>
                                </td>
                                <td>{{ $hubValue }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><strong>Actions</strong></td>
                            <td>
                                <a class="btn btn-xs btn-default" href="https://capybara_admin.test/admin/hub/2/hub"
                                   data-toggle="tooltip" title="See detailed view of hubs info and manage pylons."><i
                                            class="fa fa-search"></i> Details</a>
                                <!-- Single edit button -->
                                <a href="https://capybara_admin.test/admin/hub/2/edit" class="btn btn-xs btn-default"><i
                                            class="fa fa-edit"></i> Edit</a>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- THE ACTUAL CONTENT -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header {{ $crud->hasAccess('create')?'with-border':'' }}">

                    @include('crud::inc.button_stack', ['stack' => 'top'])

                    <div id="datatable_button_stack" class="pull-right text-right"></div>
                </div>

                <div class="box-body table-responsive">

                    <table id="crudTable" class="table table-striped table-hover display">
                        <thead>
                        <tr>
                            @if ($crud->details_row)
                                <th data-orderable="false"></th> <!-- expand/minimize button column -->
                            @endif

                            {{-- Table columns --}}
                            @foreach (Schema::getColumnListing('pylons') as $column )
                                <th data-orderable="true">
                                    {{ $column }}
                                </th>
                            @endforeach

                            @if ( $crud->buttons->where('stack', 'line')->count() )
                                <th data-orderable="false">{{ trans('backpack::crud.actions') }}</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div><!-- /.box-body -->

                @include('crud::inc.button_stack', ['stack' => 'bottom'])

            </div><!-- /.box -->
        </div>

    </div>

@endsection

@section('after_styles')
    <!-- DATA TABLES -->
    <link href="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/list.css') }}">

    <!-- CRUD LIST CONTENT - crud_list_styles stack -->
    @stack('crud_list_styles')
@endsection

@section('after_scripts')
    <!-- DATA TABLES SCRIPT -->
    <script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/form.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/list.js') }}"></script>


    <script src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {

                    @if ($crud->exportButtons())
            var dtButtons = function (buttons) {
                    var extended = [];
                    for (var i = 0; i < buttons.length; i++) {
                        var item = {
                            extend: buttons[i],
                            exportOptions: {
                                columns: [':visible']
                            }
                        };
                        switch (buttons[i]) {
                            case 'pdfHtml5':
                                item.orientation = 'landscape';
                                break;
                        }
                        extended.push(item);
                    }
                    return extended;
                }
                    @endif

            var table = $("#crudTable")
                    .on('xhr.dt', function ( e, settings, json, xhr ) {
                        console.log(json, {{ $crud->getDefaultPageLength() }});
                        // Note no return - manipulate the data directly in the JSON object.
                    } ).DataTable({
                    "pageLength": {{ $crud->getDefaultPageLength() }},
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "{{ trans('backpack::crud.all') }}"]],
                    /* Disable initial sort */
                    "aaSorting": [],
                    "language": {
                        "emptyTable": "{{ trans('backpack::crud.emptyTable') }}",
                        "info": "{{ trans('backpack::crud.info') }}",
                        "infoEmpty": "{{ trans('backpack::crud.infoEmpty') }}",
                        "infoFiltered": "{{ trans('backpack::crud.infoFiltered') }}",
                        "infoPostFix": "{{ trans('backpack::crud.infoPostFix') }}",
                        "thousands": "{{ trans('backpack::crud.thousands') }}",
                        "lengthMenu": "{{ trans('backpack::crud.lengthMenu') }}",
                        "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                        "processing": "<img src='{{ asset('vendor/backpack/crud/img/ajax-loader.gif') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                        "search": "{{ trans('backpack::crud.search') }}",
                        "zeroRecords": "{{ trans('backpack::crud.zeroRecords') }}",
                        "paginate": {
                            "first": "{{ trans('backpack::crud.paginate.first') }}",
                            "last": "{{ trans('backpack::crud.paginate.last') }}",
                            "next": "{{ trans('backpack::crud.paginate.next') }}",
                            "previous": "{{ trans('backpack::crud.paginate.previous') }}"
                        },
                        "aria": {
                            "sortAscending": "{{ trans('backpack::crud.aria.sortAscending') }}",
                            "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                        },
                        "buttons": {
                            "copy": "{{ trans('backpack::crud.export.copy') }}",
                            "excel": "{{ trans('backpack::crud.export.excel') }}",
                            "csv": "{{ trans('backpack::crud.export.csv') }}",
                            "pdf": "{{ trans('backpack::crud.export.pdf') }}",
                            "print": "{{ trans('backpack::crud.export.print') }}",
                            "colvis": "{{ trans('backpack::crud.export.column_visibility') }}"
                        },
                    },
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{!! url('/admin/hub/1/pylon/search').'?'.Request::getQueryString() !!}",
                        "type": "POST"
                    },

                        @if ($crud->exportButtons())
                    // show the export datatable buttons
                    dom:
                    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'i><'col-sm-4'B><'col-sm-4'p>>",
                    buttons: dtButtons([
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print',
                        'colvis'
                    ]),
                        @endif
                });


            // override ajax error message
            $.fn.dataTable.ext.errMode = 'none';
            $('#crudTable').on('error.dt', function (e, settings, techNote, message) {
                new PNotify({
                    type: "error",
                    title: "{{ trans('backpack::crud.ajax_error_title') }}",
                    text: "{{ trans('backpack::crud.ajax_error_text') }}"
                });
            });

            @if ($crud->exportButtons())
            // move the datatable buttons in the top-right corner and make them smaller
            table.buttons().each(function (button) {
                if (button.node.className.indexOf('buttons-columnVisibility') == -1) {
                    button.node.className = button.node.className + " btn-sm";
                }
            })
            $(".dt-buttons").appendTo($('#datatable_button_stack'));
            @endif

            $.ajaxPrefilter(function (options, originalOptions, xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-XSRF-TOKEN', token);
                }
            });

            // make the delete button work in the first result page
            register_delete_button_action();

            // make the delete button work on subsequent result pages
            $('#crudTable').on('draw.dt', function () {
                register_delete_button_action();

                @if ($crud->details_row)
                register_details_row_button_action();
                @endif
            }).dataTable();

            function register_delete_button_action() {
                $("[data-button-type=delete]").unbind('click');
                // CRUD Delete
                // ask for confirmation before deleting an item
                $("[data-button-type=delete]").click(function (e) {
                    e.preventDefault();
                    var delete_button = $(this);
                    var delete_url = $(this).attr('href');

                    if (confirm("{{ trans('backpack::crud.delete_confirm') }}") == true) {
                        $.ajax({
                            url: delete_url,
                            type: 'DELETE',
                            success: function (result) {
                                // Show an alert with the result
                                new PNotify({
                                    title: "{{ trans('backpack::crud.delete_confirmation_title') }}",
                                    text: "{{ trans('backpack::crud.delete_confirmation_message') }}",
                                    type: "success"
                                });
                                // delete the row from the table
                                delete_button.parentsUntil('tr').parent().remove();
                            },
                            error: function (result) {
                                // Show an alert with the result
                                new PNotify({
                                    title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                                    text: "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                                    type: "warning"
                                });
                            }
                        });
                    } else {
                        new PNotify({
                            title: "{{ trans('backpack::crud.delete_confirmation_not_deleted_title') }}",
                            text: "{{ trans('backpack::crud.delete_confirmation_not_deleted_message') }}",
                            type: "info"
                        });
                    }
                });
            }


            @if ($crud->details_row)
            function register_details_row_button_action() {
                // var crudTable = $('#crudTable tbody');
                // Remove any previously registered event handlers from draw.dt event callback
                $('#crudTable tbody').off('click', 'td .details-row-button');

                // Make sure the ajaxDatatables rows also have the correct classes
                $('#crudTable tbody td .details-row-button').parent('td')
                    .removeClass('details-control').addClass('details-control')
                    .removeClass('text-center').addClass('text-center')
                    .removeClass('cursor-pointer').addClass('cursor-pointer');

                // Add event listener for opening and closing details
                $('#crudTable tbody td.details-control').on('click', function () {
                    var tr = $(this).closest('tr');
                    var btn = $(this).find('.details-row-button');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        btn.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                        $('div.table_row_slider', row.child()).slideUp(function () {
                            row.child.hide();
                            tr.removeClass('shown');
                        });
                    }
                    else {
                        // Open this row
                        btn.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                        // Get the details with ajax
                        $.ajax({
                            url: '{{ url($crud->route) }}/' + btn.data('entry-id') + '/details',
                            type: 'GET',
                            // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                            // data: {param1: 'value1'},
                        })
                            .done(function (data) {
                                // console.log("-- success getting table extra details row with AJAX");
                                row.child("<div class='table_row_slider'>" + data + "</div>", 'no-padding').show();
                                tr.addClass('shown');
                                $('div.table_row_slider', row.child()).slideDown();
                                register_delete_button_action();
                            })
                            .fail(function (data) {
                                // console.log("-- error getting table extra details row with AJAX");
                                row.child("<div class='table_row_slider'>{{ trans('backpack::crud.details_row_loading_error') }}</div>").show();
                                tr.addClass('shown');
                                $('div.table_row_slider', row.child()).slideDown();
                            })
                            .always(function (data) {
                                console.log("-- complete getting table extra details row with AJAX");
                            });
                    }
                });
            }

            register_details_row_button_action();
            @endif


        });
    </script>

    <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
    @stack('crud_list_scripts')
    </div>

@endsection
