@extends('backpack::layout')

@section('content')


    <!-- Device List Box -->
    <div class="box manage">
        <div class="box-header with-border">
            <div>
                <h3 class="box-title">
                    Devices | Unassociated with a hub
                </h3>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach($devices as $device)
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>Device ID: {{ $device->id }}</h3>

                                <p>Device Name: {{ $device->name }}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-asterisk"></i>
                            </div>
                            <a href="#" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="box box-default collapsed-box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Devices | All ( {{ \App\Device::all()->count() }} )</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">Id</th>
                    <th>Name</th>
                    <th>Sensor Count</th>
                    <th>Reading Count</th>
                </tr>
                @foreach(\App\Device::all() as $device)
                    <tr>
                        <td>{{ $device->id }}</td>
                        <td>{{ $device->name }}</td>
                        <td>{{ isset($device->readings()->first()->sensors) ? count($device->readings()->first()->sensors) : 'device might not be setup correctly'}}</td>
                        <td>{{ $device->readings()->get() ? count($device->readings()->get()) : 'device might not be setup correctly'}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

