<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;


Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/hub/{hub}/pylon/{pylon}/sensor/search', 'Admin\PylonSensorController@search');

Route::get('admin/test', 'Admin\HubController@test');
// Admin Interface Routes
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function()
{
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::get('/dashboard/{hub}', 'Admin\DashboardController@show');


    // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('pylon', 'Admin\PylonController');
    CRUD::resource('sensor_type', 'Admin\SensorTypeController');
    CRUD::resource('sensor', 'Admin\SensorController');
    CRUD::resource('hub', 'Admin\HubController');
    Route::get('/hub/{hub}/manage', 'Admin\HubController@manageHubView');
    Route::get('/hub/{hub}/pylon/{pylon}/manage', 'Admin\PylonController@manageHubView');

    Route::group(['prefix' => 'hub/{hub}'], function()
    {
        CRUD::resource('pylon', 'Admin\HubPylonController');
        Route::get('pylon', function (Request $request){
            return redirect('admin/hub/'. $request->route('hub') .'/manage');
        });

        CRUD::resource('pylon/{pylon}/sensor', 'Admin\PylonSensorController');
        Route::get('pylon/{pylon}/sensor', function (Request $request){
            return redirect('admin/hub/'. $request->route('hub') .'pylon/' . $request->route('pylon') . '/sensor/manage');
        });
    });

    Route::get('device', 'Admin\DeviceController@index');
    Route::get('device/{device}/', 'Admin\DeviceController@show');

    Route::post('api/hub/{hub}/readings', 'API\HubReadings@index');


});
